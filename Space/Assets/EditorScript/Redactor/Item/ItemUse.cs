﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemUse : int
{
    None = 0,
    Weapon = 1,
    Armor = 2,
    RestorePoints = 3
}

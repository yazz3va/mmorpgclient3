﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Items
{
    [System.Serializable]
    public class RestorePointsItem
    {
        public int hp;
        public int stamina;
        public int mp;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MonsterRedactor
{
    public class MonsterEditor : ScriptableObject
    {
        public Monster monster;
    }
}
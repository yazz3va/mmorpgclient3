﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillsRedactor
{
    [System.Serializable]
    public class MelleSkill
    {
        public float damage;
        public float prickingDamage;
        public float crushingDamage;
        public float choppingDamage;
        public float range;
        public float angle;
        public int stamina;
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillsRedactor {
    public class SkillEditor : ScriptableObject
    {
        public System.Object serializableObject;
        public Skill skill;
    }
}
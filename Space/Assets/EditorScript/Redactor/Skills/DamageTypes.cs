﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SkillsRedactor
{
    public enum DamageTypes
    {
        None,
        Pricking,
        Crushing,
        Chopping
    }
}
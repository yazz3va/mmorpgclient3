﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillsRedactor
{
    public enum SkillType:byte
    {
        None = 0,
        Melee = 1,
        Teleport = 2
    }
}
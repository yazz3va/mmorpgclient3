﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Animation
{
    public enum AnimationType:byte
    {

        Atack0 = 0,
        Atack1 = 1,
        Atack2 = 2,
        Atack3 = 3
    }
}
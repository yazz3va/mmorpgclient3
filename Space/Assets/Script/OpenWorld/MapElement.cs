﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace OpenWorld
{
    public class MapElement : ScriptableObject
    {

        public TerrainData terrainData;
        public Mesh waterTile;
    }
}
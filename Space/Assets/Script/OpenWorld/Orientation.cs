﻿namespace OpenWorld { 
    public enum Orientation
    {
        Left,
        Right,
        Up,
        Down
    }
}